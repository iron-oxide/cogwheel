//! factor.rs
#![deny(missing_docs)]

use std::vec::Vec;


/// The Pollard-Strassen factorization method.
///
/// The fastest-known fully proven deterministic algorithm.
///
pub fn poll_strass(n: u64) {
    unimplemented!()
}


/// elliptic curve factorization method (ECM)
///
/// Also known as the Lenstra elliptic curve factorization, is the
/// third-fastest known factoring method. it is still the best
/// algorithm for divisors not greatly exceeding 20 to 25 digits
/// (64 to 83 bits or so), as its running time is dominated by the size
/// of the smallest factor p rather than by the size of the number n to be
/// factored. Frequently, ECM is used to remove small factors from a very
/// large integer with many factors; if the remaining integer is still
/// composite, then it has only large factors and is factored using general
/// purpose techniques.
///
pub fn ecm(n: u64) {
    unimplemented!()
}


/// Trial division factorization method.
///
/// Simplest factoring algorithm. For each k 2, 3, 4, 5, ... tests if n
/// is divisible by k and stops at the square root of n.
///
/// Worst Case:
///
/// > O(n^(1/2))
///
/// Size of input:
///
/// > O(log n) bits
///
/// Only practical for very small n.
///
pub fn trial_div(n: u64) -> Vec<u64> {
    let limit: u64 = ((n as f64) / 2.0).round() as u64;
    let mut factors = vec![1];
    let mut k: u64 = 2;
    while k <= limit {
        if n % k == 0 {
            factors.push(k);
        }
        k += 1;
    }
    factors.push(n);
    factors
}


#[cfg(test)]
mod tests {
    use super::trial_div;
    #[test]
    fn simple_trial_div() {
        assert_eq!(trial_div(10), vec![1, 2, 5, 10]);
        assert_eq!(trial_div(72), vec![1, 2, 3, 4, 6, 8, 9, 12, 18, 24, 36, 72]);
    }
}
