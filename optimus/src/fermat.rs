//! Fermat's Prime Factorization Method.
//!
#![deny(missing_docs)]


/// Fermat's Method
///
/// Finds n = ((a-b)/2)((a+b-2)/2)
///
/// `n` must be odd.
///
/// ((a-b)/2) is the largest prime factor of `n` less than or
/// equal to `n.sqrt()`.
///
/// TODO:
///
/// Handle even case with Option
///
pub fn fermat(n: i64) -> Vec<i64> {

    assert_ne!(0, n % 2);

    // Initialize
    let mut a: i64 = 2 * (n as f64).sqrt().ceil() as i64 + 1;
    let mut b: i64 = 1;
    let mut r: i64 = ((n as f64).sqrt().ceil() as i64).pow(2) - n;
    let mut factors = vec![];

    // Done?
    if r == 0 {
        factors.push((a - b) / 2);
        factors.push((a + b - 2) / 2);
        return factors;
    }

    loop {
        // Increase a
        r = r + a;
        a = a + 2;

        // Increase b
        r = r - b;
        b = b + 2;

        while r > 0 {
            r = r - b;
            b = b + 2;
        }

        // Done?
        if r == 0 {
            factors.push((a - b) / 2);
            factors.push((a + b - 2) / 2);
            break;
        }
    }

    factors
}


#[cfg(test)]
mod tests {
    use super::fermat;
    #[test]
    fn simple_fermat() {
        let empty: Vec<i64> = Vec::new();
        assert_eq!(vec![59, 101], fermat(5959));
        assert_eq!(vec![1, 7], fermat(7));
        assert_eq!(vec![1, 190823], fermat(190823));
        assert_eq!(vec![89681, 96079], fermat(8616460799));
        assert_eq!(vec![103591, 104729], fermat(103591 * 104729));
    }
}
