//! Trial Division Factorization Method.
//!
//! Finding prime factors by division.
#![deny(missing_docs)]

use primality::is_prime;

/// Trial division factorization method.
///
/// > O(sqrt(n))
///
/// Only practical for very small n.
///
/// # Examples
///
/// If p * q = n then the possible cases are:
///
/// 1. The given number is prime and has only itself and 1 as factors. (1,n)
/// 2. The given number is a square of a prime
/// 3. The given number is a product of just two prime numbers p & q
/// 4. The given number has again two prime factors, but now, and at-least one of them is repeated
/// 5. The given number has more than two prime factors, each with/without repetition
///
/// --------------------------------------------------------------------
///
/// 1. No other prime factors
///
///   ```
///   use optimus::division::trial_div;
///
///   assert_eq!(vec![11], trial_div(11));
///   ```
///
/// 2. p = q = sqrt(n)
///
///   ```
///   use optimus::division::trial_div;
///
///   assert_eq!(vec![5, 5], trial_div(25));
///   ```
///
/// 3. One of p, q will be < sqrt(n) and the other will be > sqrt(n)
///
///   ```
///   use optimus::division::trial_div;
///
///   assert_eq!(vec![2, 7], trial_div(14));
///   ```
///
/// 4. With repetition of factors at most one out of p, q could be > sqrt(n)
///
///   ```
///   use optimus::division::trial_div;
///
///   assert_eq!(vec![2, 2, 7], trial_div(28));
///   ```
///
/// 5. Without repetition of factors at most one of p, q could be > sqrt(n)
///
///   ```
///   use optimus::division::trial_div;
///
///   assert_eq!(vec![2, 2, 3, 7], trial_div(84));
///   ```
///
/// Only the cases 1, 2, and 3 always touch or cross the sqrt(n) border.
///
/// - Case 1 is crossing because its only factor is itself
/// - Case 2 is touching sqrt(n), but it can't cross
/// - Case 3 does cross sqrt(n) because 2 < sqrt(n) and 7 > sqrt(n)
/// - Case 4 & 5 could cross sqrt(n) sometimes, but not always
///
/// Think of square-root as splitting a number into two groups in the
/// multiplicative sense.
///
/// 1. You cant split it into groups
/// 2. Splits into two
/// 3. Can't split perfectly as the number is not a square, one side dominates.
/// 4. Splits more than twice into small pieces, consider this if p * p is itself < sqrt(n) q could still be > sqrt(n)
/// 5. Splits more than twice into small pieces, one piece could still become greater than sqrt(n) like in case 4 above
///
/// If n is not a perfect square, factors can be bunched up so that one chain
/// will be < sqrt(n) and the other chain will be > sqrt(n). If the chain that
/// is > sqrt(n) has only one member, in that case the prime-factor that
/// is > sqrt(n) exists.
///
pub fn trial_div(n: u64) -> Vec<u64> {

    let mut k: u64 = n;
    let mut factors = vec![];

    // Eliminate all even numbers
    while k % 2 == 0 {
        factors.push(2);
        k /= 2;
    }

    // Find factors up to sqrt(n)
    let mut i: u64 = 3;
    while i <= (n as f64).sqrt().floor() as u64 {
        if k % i == 0 {
            while k % i == 0 {
                factors.push(i);
                k /= i;
            }
        }
        i += 2;
    }

    // Test if factor exists above sqrt(n)
    let mut f: u64 = 0;
    let mut combined_factors = vec![];
    for factor in factors.iter() {
        let mut factor_count: u64 = 0;
        for num in factors.iter() {
            if factor == num {
                factor_count += 1;
            }
        }
        combined_factors.push(factor.pow(factor_count as u32));
    }
    for factor in combined_factors {
        if is_prime(n / factor) && (n / factor) > (n as f64).sqrt() as u64 {
            f = factor;
            break;
        }
    }
    if f != 0 {
        factors.push(n / f);
    }
    if is_prime(n) {
        factors.push(n);
    }

    factors
}


#[cfg(test)]
mod tests {
    use super::trial_div;
    #[test]
    fn simple_trial_div() {
        let empty: Vec<u64> = Vec::new();
        assert_eq!(vec![7], trial_div(7));
        assert_eq!(vec![2, 5], trial_div(10));
        assert_eq!(vec![2, 7], trial_div(14));
        assert_eq!(vec![2, 2, 2, 3, 3], trial_div(72));
        assert_eq!(vec![2, 2, 5, 5], trial_div(100));
        assert_eq!(vec![2, 2, 2, 5, 5, 5], trial_div(1000));
        assert_eq!(vec![5, 7, 13, 29], trial_div(13195));
        assert_eq!(vec![190823], trial_div(190823));
        assert_eq!(vec![71, 839, 1471, 6857], trial_div(600851475143));
    }

}
