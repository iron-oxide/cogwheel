//! The Pollard Rho Prime Factorization Method.
//!
//! Finds the prime factors of an integer N >= 2, with high probability,
//! although it may fail.
#![deny(missing_docs)]

extern crate num;
use self::num::Integer;
use primality::is_prime;

/// Pollard Rho
///
/// Factoring by pseudorandom cycles.
///
/// Variables:
///
/// `N` is the unfactored part of `n`
///
/// `x` and `x2` represent the quantities `x_m mod n` and
/// `x_{l(m)}-1 mod n` in (10), where f(x) = x^2 + 1, A = 2, l = l(m),
/// and k = 2l-m.
///
/// TODO:
///
/// The function `(x.pow(2) + 2) % n` or x^2 + c used to generate
/// the random mapping can be changed in case of failure. This may
/// work to prevent failure and produce a result.
///
pub fn pollard_rho(mut n: i64) -> Vec<i64> {

    // Initialize
    let mut g: i64;
    let mut x: i64 = 5;
    let mut x2: i64 = 2;
    let mut k: i64 = 1;
    let mut l: i64 = 1;
    let mut factors = vec![];

    // Test Primality
    if is_prime(n as u64) {
        factors.push(n);
        return factors;
    }

    loop {
        // Found Factor?
        g = (x2 - x).gcd(&n);
        if g == n {
            panic!("Pollard Rho method failed")
        } else if g == 1 {
            // Advance
            k = k - 1;
            if k == 0 {
                x2 = x;
                l = 2 * l;
                k = l;
            }
            x = (x.pow(2) + 2) % n
        } else {
            factors.push(g);
            n = n / g;
            x = x % n;
            x2 = x2 % n;
            // Test Primality
            if is_prime(n as u64) {
                factors.push(n);
                break;
            }
        }
    }

    factors.sort();
    factors
}


#[cfg(test)]
mod tests {
    use super::pollard_rho;
    #[test]
    fn simple_pollard_rho() {
        let empty: Vec<i64> = Vec::new();
        assert_eq!(vec![7], pollard_rho(7));
        assert_eq!(vec![2, 5], pollard_rho(10));
        assert_eq!(vec![2, 7], pollard_rho(14));
        assert_eq!(vec![2, 2, 2, 3, 3], pollard_rho(72));
        assert_eq!(vec![2, 2, 5, 5], pollard_rho(100));
        assert_eq!(vec![2, 2, 2, 5, 5, 5], pollard_rho(1000));
        assert_eq!(vec![5, 7, 13, 29], pollard_rho(13195));
        assert_eq!(vec![190823], pollard_rho(190823));
        // attempted multiply with overflow needs bigint
        //        assert_eq!(vec![71, 839, 1471, 6857], pollard_rho(600851475143));
    }

}
