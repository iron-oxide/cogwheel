//! Prime Sieves
//!
#![deny(missing_docs)]

use bit_vec::BitVec;

/// Sieve of Eratosthenes
///
/// Clasical:
///
/// ```ignore
///     O(n log log n) and O(n) bits of storage.
/// ```
///
/// Optimized:
///
/// ```ignore
///     O(n) and O(sqrt(n)) bits of storage.
/// ```
///
/// Uses a bit vector to store which numbers are prime. To avoid
/// waisted computation and space, the array starts at 3 and indexes
/// by 2. This must be taken into account when accessing the data
/// stored by bit index.
///
/// The algorithm assumes all numbers prime, then crosses off the
/// multiples. The formula to get the index for a given `n` is:
///
/// ```ignore
///     n - # of indexs skipped
///     -----------------------
///                2
/// ```
///
/// Skipping indices (0, 1, 2) gives:
///
/// ```ignore
///     n - 3
///     -----
///       2
/// ```
///
/// And in reverse index to number:
///
/// ```ignore
///     i * 2 + 3 = n
/// ```
///
/// In the inner loop even multipes are skipped eliminating
/// the need for checks there also. Because an odd number
/// times an even number is always even they can be skipped
/// in the same manner as the outer loop.
///
pub fn eratosthenes(n: u64) -> Vec<u64> {

    // Initialize
    let upper_bound = (n as f64).sqrt().ceil() as u64;
    let mut bv = BitVec::from_elem(n as usize, true);
    let mut primes = vec![];

    if n <= 1 {
        return primes;
    }

    if n == 2 {
        return vec![2];
    }

    let mut i: u64 = 0;
    let mut base_num = 3;
    while base_num <= upper_bound {
        // If prime cross off all multiples
        if bv[i as usize] {
            let mut multiple: u64 = base_num;
            while base_num * multiple <= n {
                let number = base_num * multiple;
                // convert number to index
                bv.set(((number - 3) / 2) as usize, false);
                multiple += 2;
            }
        }
        i += 1;
        base_num += 2;
    }
    
    primes.push(2);
    for (index, prime) in bv.iter().enumerate() {
        if prime {
            let number = index as u64 * 2 + 3;
            if number > n {
                break;
            }
            // convert index to number
            primes.push(index as u64 * 2 + 3);
        }
    }

    primes
}


#[cfg(test)]
mod tests {

    extern crate csv;
    use std::path::PathBuf;
    use super::eratosthenes;

    static primes_under_n: &'static str = "resources/primes_under_n/";

    #[test]
    fn simple_eratosthenes() {
        let empty: Vec<u64> = Vec::new();
        assert_eq!(empty, eratosthenes(0));
        assert_eq!(empty, eratosthenes(1));
        assert_eq!(vec![2], eratosthenes(2));
        assert_eq!(vec![2, 3], eratosthenes(3));
        assert_eq!(vec![2, 3], eratosthenes(4));
        assert_eq!(vec![2, 3, 5], eratosthenes(5));
        assert_eq!(vec![2, 3, 5], eratosthenes(6));
        assert_eq!(vec![2, 3, 5, 7], eratosthenes(7));
        assert_eq!(vec![2, 3, 5, 7], eratosthenes(8));
        assert_eq!(vec![2, 3, 5, 7], eratosthenes(9));
        assert_eq!(vec![2, 3, 5, 7], eratosthenes(10));
        assert_eq!(vec![2, 3, 5, 7, 11], eratosthenes(11));
    }
    
    #[test]
    fn eratosthenes_primes_under_one_hundred() {
        let mut primes_under_one_hundred: Vec<u64> = vec![];

        let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_path.push(primes_under_n);
        file_path.push("primes_under_one_hundred.csv");

        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(file_path)
            .unwrap();

        for record in rdr.deserialize() {
            let prime: Vec<u64> = record.unwrap();
            primes_under_one_hundred.extend(prime);
        }

        assert_eq!(primes_under_one_hundred, eratosthenes(100))
    }

    #[test]
    fn eratosthenes_primes_under_one_thousand() {
        let mut primes_under_one_thousand: Vec<u64> = vec![];

        let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_path.push(primes_under_n);
        file_path.push("primes_under_one_thousand.csv");

        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(file_path)
            .unwrap();

        for record in rdr.deserialize() {
            let prime: Vec<u64> = record.unwrap();
            primes_under_one_thousand.extend(prime);
        }

        assert_eq!(primes_under_one_thousand, eratosthenes(1000))
    }

    #[test]
    fn eratosthenes_primes_under_eight_thousand() {
        let mut primes_under_eight_thousand: Vec<u64> = vec![];

        let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_path.push(primes_under_n);
        file_path.push("primes_under_eight_thousand.csv");

        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(file_path)
            .unwrap();

        for record in rdr.deserialize() {
            let prime: Vec<u64> = record.unwrap();
            primes_under_eight_thousand.extend(prime);
        }

        assert_eq!(primes_under_eight_thousand, eratosthenes(8000))
    }

    #[test]
    fn eratosthenes_primes_under_one_million() {
        let mut primes_under_one_million: Vec<u64> = vec![];

        let mut file_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        file_path.push(primes_under_n);
        file_path.push("primes_under_one_million.csv");

        let mut rdr = csv::ReaderBuilder::new()
            .has_headers(false)
            .from_path(file_path)
            .unwrap();

        for record in rdr.deserialize() {
            let prime: Vec<u64> = record.unwrap();
            primes_under_one_million.extend(prime);
        }

        assert_eq!(primes_under_one_million, eratosthenes(1000000))
    }
}
