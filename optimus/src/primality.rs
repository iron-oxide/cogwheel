//! Determining Primality.
//!
//! Determining if a number is prime.
#![deny(missing_docs)]


/// Test if a number is prime.
///
/// - The number 1 is not prime.
/// - The number 2 is prime. (It is the only even prime.)
///   - Checking just 2 eliminates all even numbers
///
/// Start at the end of base cases (3) and test each number to see if
/// `n` is divisible by it. Skip even numbers since the case has
/// already been tested, and return false if any divisor is found.
///
pub fn is_prime(n: u64) -> bool {
    if n <= 1 {
        return false;
    } else if n == 2 {
        return true;
    } else if n % 2 == 0 {
        return false;
    }
    let mut i: u64 = 3;
    while i <= (n as f64).sqrt().floor() as u64 {
        if n % i == 0 {
            return false;
        }
        i += 2;
    }
    true
}


#[cfg(test)]
mod tests {
    use super::is_prime;
    #[test]
    fn simple_is_prime() {
        assert_eq!(false, is_prime(0));
        assert_eq!(false, is_prime(1));
        assert_eq!(true, is_prime(2));
        assert_eq!(true, is_prime(3));
        assert_eq!(false, is_prime(4));
        assert_eq!(true, is_prime(5));
        assert_eq!(false, is_prime(6));
        assert_eq!(true, is_prime(7));
        assert_eq!(false, is_prime(8));
        assert_eq!(false, is_prime(9));
        assert_eq!(false, is_prime(10));
        assert_eq!(true, is_prime(11));
    }
}
