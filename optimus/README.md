# Optimus

Number theoretical prime-number library.

## TODO:

- [] Segment sieve
- [] Keep bit-vector as underlying data structure
    - [] Optional conversion to `std::Vec`
    - [] Optional conversion to `std::array`
- [] Optimise bit-vector
    - [] Estimate capacity `π(x)` (upper bound) needed for `n` ~ `1/(ln(n)-1)`[^1]
- [] Optimise modulo
- [] Implement Miller-Rabin primality test [^2]

-------------------------------------------------------------

[^1]: [https://primes.utm.edu/howmany.html](https://primes.utm.edu/howmany.html)
[^1]: [https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test](https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test)
