#![feature(test)]

extern crate test;
use test::Bencher;

extern crate optimus;
use optimus::sieve::eratosthenes;

#[bench]
fn eratosthenes_bench_one_thousand(b: &mut Bencher) {
    b.iter(|| {
        let n = test::black_box(1000);

        eratosthenes(n)
    })
}

#[bench]
fn eratosthenes_bench_one_million(b: &mut Bencher) {
    b.iter(|| {
        let n = test::black_box(1000000);

        eratosthenes(n)
    })
}

#[bench]
fn eratosthenes_bench_ten_million(b: &mut Bencher) {
    b.iter(|| {
        let n = test::black_box(10000000);

        eratosthenes(n)
    })
}

/* Taking too long
 *
 * Consider enabling whenever it is possible to limit the number
 * of iterations to less than 301.
 *
#[bench]
fn eratosthenes_bench_one_hundred_million(b: &mut Bencher) {
    b.iter(|| {
        let n = test::black_box(100000000);

        eratosthenes(n)
    })
}

#[bench]
fn eratosthenes_bench_one_billion(b: &mut Bencher) {
    b.iter(|| {
        let n = test::black_box(1000000000);

        eratosthenes(n)
    })
}
*/
