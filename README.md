# Cogwheel

[![codecov](https://codecov.io/gl/iron-oxide/cogwheel/branch/master/graph/badge.svg)](https://codecov.io/gl/iron-oxide/cogwheel)

A Rust math library for fun and use with [Project Euler](https://projecteuler.net/), [Cryptopals](https://cryptopals.com/) and the like.

## How to use

Build, test, build docs:

    cargo build
    cargo test
    cargo doc

Docs can be found [here](http://iron-oxide.gitlab.io/cogwheel/cogwheel/index.html).

## The book

    cargo install mdbook
    cd lovelace
    mdbook build
    mdbook serve
